# -----------------------------------------------------------------
# Fifth labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

import math
import random as rand
import time

# Task 1 !!!

print("{:_^40}".format(" Task 1 "))


def calc_rect_area(side_a, side_b):
    return side_a * side_b


for i in range(3):
    first_side = int(input("First side: "))
    second_side = int(input("Second side: "))
    print(calc_rect_area(first_side, second_side), 'cm2\n')

# Task 2

print("{:_^40}".format(" Task 2 "))


def calc_hypoten_by_legs(leg_a, leg_b):
    return math.sqrt(leg_a ** 2 + leg_b ** 2)


for i in range(2):
    first_leg = int(input("First leg: "))
    second_leg = int(input("Second leg: "))
    print(round(calc_hypoten_by_legs(first_leg, second_leg), 3), 'cm\n')

# Task 3

print("{:_^40}".format(" Task 3 "))
circle_x, circle_y = int(input("Circle X: ")), int(input("Circle Y: "))
circle_r = int(input("Circle radius: "))


def is_circle_point(x, y, a, b, radius):
    if (x - a) ** 2 + (y - b) ** 2 <= radius ** 2:
        return True
    else:
        return False


for i in range(3):
    point_x, point_y = int(input("Point X: ")), int(input("Point Y: "))
    print(is_circle_point(circle_x, circle_y, point_x, point_y, circle_r))

# Task 4

print("{:_^40}".format(" Task 4 "))


def calc_rec_area(x_site, y_site, z_site, t_site):
    diagonal = math.sqrt(x_site ** 2 + y_site ** 2)
    area_small_triangle = 0.5 * x_site * y_site
    triangle_per = (z_site + t_site + diagonal) / 2
    area_big_triangle = triangle_per * (triangle_per - z_site)
    area_big_triangle *= (triangle_per - t_site) * (triangle_per - diagonal)
    return area_small_triangle + math.sqrt(area_big_triangle)


x_side, y_side = int(input("Enter x length: ")), int(input("Enter y len: "))
z_side, t_side = int(input("Enter z length: ")), int(input("Enter t len: "))
area = round(calc_rec_area(x_side, y_side, z_side, t_side), 3)
print(f"Rectangle area {area} cm2")

# Task 5

print("{:_^40}".format(" Task 5 "))


def gen_natural_nums(max_natural_num):
    return [i for i in range(1, max_natural_num + 1)]


n_num = int(input("Enter n number: "))
count_nums = int(input("Count of division numbers: "))
users_nums = [int(input("Number: ")) for i in range(count_nums)]


def check_div_el_to_elements(el, nums_list):
    for i in nums_list:
        if el % i == 0:
            return True
        else:
            return False


sorted_list = [i for i in gen_natural_nums(n_num) if
               check_div_el_to_elements(i, users_nums)]

print(f"List {sorted_list}")

# Task 6

print("{:_^40}".format(" Task 6 "))


def fnd_max_divs_nums(nums: list):
    max_divs_num = 0
    max_divs_nums_list = []
    for i in nums:
        max_divs_on_el = 0
        for y in nums:
            if i % y == 0:
                max_divs_on_el += 1
            if max_divs_num < max_divs_on_el:
                max_divs_num = int(max_divs_on_el)
                max_divs_nums_list.clear()
            if max_divs_on_el == max_divs_num:
                if i not in max_divs_nums_list:
                    max_divs_nums_list.append(i)
    return max_divs_nums_list, max_divs_num


start_val, end_val = int(input("Start val: ")), int(input("End val: "))
nums_list = [i for i in range(start_val, end_val + 1)]
max_divs_nums = fnd_max_divs_nums(nums_list)
print(f"Elements {max_divs_nums[0]} max count of divs {max_divs_nums[1]}")

# Task 7

print("{:_^35}".format(" Task 7 "))


def fnd_simple_nums(nums: list):
    simple_nums_list = []
    for i in nums:
        simple_num = True
        for y in nums:
            if i % y == 0 and i != y and i > 1 and y != 1:
                simple_num = False
        if simple_num and i != 1:
            simple_nums_list.append(i)
    return simple_nums_list


def print_list(nums: list):
    print("Variants: 1 - list 2 - downline 3 - count")
    variant = int(input("Enter variant: "))
    match variant:
        case 1:
            print(nums)
        case 2:
            for i in nums:
                print(i)
        case 3:
            print(len(nums))


max_num = int(input("Max num: "))
nums_list = [i for i in range(1, max_num + 1)]
print_list(fnd_simple_nums(nums_list))

# Task 8

print("{:_^40}".format(" Task 8 "))


def insert_list_nums(nums: list, bot_indx, up_indx, min_num, max_num):
    inserted_list = [i for i in nums[bot_indx:up_indx] if
                     min_num < i < max_num]
    return inserted_list


flag = True
while flag:
    try:
        list_length = int(input("Length natural random nums list: "))
        nums_list = [rand.randint(-1000, 1000) for i in range(list_length)]
        max_num = int(input("Max new list num ( < 1000 ): "))
        min_num = int(input("Min new list num ( > -1000 ): "))
        if max_num > 1000 or min_num < -1000:
            raise OverflowError
        bottom_num = int(input("Bottom num: "))
        upper_num = int(input("Upper num: "))
        if bottom_num < 0 and upper_num > list_length:
            raise OverflowError
    except ValueError:
        print("Enter the int number!\n")
    except OverflowError:
        print("Incorrect max or min or bot or up number!\n")
    else:
        flag = False
        nums_list = insert_list_nums(nums_list, bottom_num,
                                     upper_num, min_num, max_num)
        print(f"List {nums_list}")

# Task 9

print("{:_^40}".format(" Task 9 "))


def calc_exec_time_decorator(func):
    def new_func(*args):
        start_time = time.time()
        ret_val = func(*args)
        end_time = time.time()
        exec_time = end_time - start_time
        print(f"Execution time = {round(exec_time, 5)} seconds")
        return ret_val

    return new_func


# 9.6

@calc_exec_time_decorator
def fnd_max_divs_nums(nums: list):
    max_divs_num = 0
    max_divs_nums_list = []
    for i in nums:
        max_divs_on_el = 0
        for y in nums:
            if i % y == 0:
                max_divs_on_el += 1
            if max_divs_num < max_divs_on_el:
                max_divs_num = int(max_divs_on_el)
                max_divs_nums_list.clear()
            if max_divs_on_el == max_divs_num:
                if i not in max_divs_nums_list:
                    max_divs_nums_list.append(i)
    return max_divs_nums_list


# n = 4
start_val, end_val = int(input("Start val: ")), int(input("End val: "))
nums_list = [i for i in range(start_val, (end_val + 1) ** 4)]
max_divs_nums = fnd_max_divs_nums(nums_list)
print(f"Elements {max_divs_nums[0]}")

# 9.7 !!!

@calc_exec_time_decorator
def fnd_simple_nums(nums: list):
    simple_nums_list = []
    for i in nums:
        simple_num = True
        for y in nums:
            if i % y == 0 and i != y and i > 1 and y != 1:
                simple_num = False
        if simple_num and i != 1:
            simple_nums_list.append(i)
    return simple_nums_list


def print_list(nums: list):
    print("Variants: 1 - list 2 - downline 3 - count")
    variant = int(input("Enter variant: "))
    match variant:
        case 1:
            print(nums)
        case 2:
            for i in nums:
                print(i)
        case 3:
            print(len(nums))


# n = 4
max_num = int(input("Max num: "))
nums_list = [i for i in range(1, (max_num + 1) ** 4)]
print_list(fnd_simple_nums(nums_list))

# 9.8

@calc_exec_time_decorator
def insert_list_nums(nums: list, bot_indx, up_indx, min_num, max_num):
    inserted_list = [i for i in nums[bot_indx:up_indx] if
                     min_num < i < max_num]
    return inserted_list


# n = 8
flag = True
while flag:
    try:
        list_length = int(input("Length natural random nums list: ")) ** 8
        nums_list = [rand.randint(-1000, 1000) for i in range(list_length)]
        max_num = int(input("Max new list num ( < 1000 ): "))
        min_num = int(input("Min new list num ( > -1000 ): "))
        if max_num > 1000 or min_num < -1000:
            raise OverflowError
        bottom_num = int(input("Bottom num: "))
        upper_num = int(input("Upper num: "))
        if bottom_num < 0 and upper_num > list_length:
            raise OverflowError
    except ValueError:
        print("Enter the int number!\n")
    except OverflowError:
        print("Incorrect max or min or bot or up number!\n")
    else:
        flag = False
        nums_list = insert_list_nums(nums_list, bottom_num,
                                     upper_num, min_num, max_num)
        # print(f"List {nums_list}")