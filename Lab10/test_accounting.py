# -----------------------------------------------------------------
# Tan labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

import pytest
from src import accounting as ac


# Test

@pytest.fixture
def user():
    return ac.User('Denys', 'Ataman', 'den@gmail.com', 'Ataman', True)


def test_user(user, capsys):
    assert user.first_name == 'Denys'
    assert user.last_name == 'Ataman'
    assert user.email == 'den@gmail.com'
    assert user.nickname == 'Ataman'
    assert user.is_news_accepted is True
    assert user.get_login_attempts() == 0

    user.describe_user()
    captured = capsys.readouterr()
    assert 'User Denys Ataman' in captured.out

    user.greeting_user()
    captured = capsys.readouterr()
    assert 'We are glad to see you Denys' in captured.out

    user.increment_login_attempts()
    user.increment_login_attempts()
    assert user.get_login_attempts() == 2

    user.reset_login_attempts()
    assert user.get_login_attempts() == 0


@pytest.mark.parametrize('privilege', [
    'Allowed to add message',
    'Allowed to delete users',
    'Allowed to ban users',
    'Allowed to post products'
])
def test_privileges(privilege, capsys):
    my_privilege = ac.Privileges([privilege])
    captured = capsys.readouterr()
    my_privilege.show_privileges()
    assert captured.out in privilege


@pytest.fixture
def admin():
    privileges = ac.Privileges(['Allowed to delete users',
                               'Allowed to ban users'])
    return ac.Admin('Oleg', 'Adminov', 'o@gmail.com', '-', privileges)


def test_admin(admin, capsys):
    assert admin.first_name == 'Oleg'
    assert admin.last_name == 'Adminov'
    assert admin.email == 'o@gmail.com'
    assert admin.nickname == '-'
    assert admin.is_news_accepted is False
    assert admin.get_login_attempts() == 0

    captured = capsys.readouterr()
    admin.privileges.show_privileges()
    assert captured.out in 'Allowed to delete users'
