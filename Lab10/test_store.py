# -----------------------------------------------------------------
# Ten labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

import pytest
from src import store


# Test

@pytest.fixture
def my_shop():
    return store.Shop("My Shop", "Grocery")


def test_shop_description(my_shop, capsys):
    my_shop.describe_shop()
    captured = capsys.readouterr()
    assert "Shop My Shop has Grocery store type" in captured.out


def test_shop_opening(my_shop, capsys):
    my_shop.open_shop()
    captured = capsys.readouterr()
    assert "Shop has opened today" in captured.out


def test_shop_number_of_units(my_shop):
    my_shop.set_number_of_units(10)
    assert my_shop.number_of_units == 10
    my_shop.increment_number_of_units(5)
    assert my_shop.number_of_units == 15


def test_discount_products():
    discount_products = ["product1", "product2", "product3"]
    discount_shop = store.Discount(discount_products,
                                   "Discount Shop", "Grocery")
    assert discount_shop.get_discount_products() == discount_products
