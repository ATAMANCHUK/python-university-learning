class Alphabet:
    lang = 'UA'
    letters = ['А', 'Б', 'В', 'Г', 'Ґ', 'Д', 'Е', 'Є', 'Ж', 'З', 'И',
               'І', 'Ї', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
               'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ю', 'Я']

    def __init__(self, lang=lang, letters=letters):
        self.lang = lang
        self.letters = letters

    def print_alphabet(self):
        print(self.letters)

    def letters_num(self):
        print(len(self.letters))

    def is_ua_lang(self, text: str):
        text = text.replace(' ', '')
        return all([letter.upper() in self.letters for letter in text])


class EngAlphabet(Alphabet):
    lang = 'EN'
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    __en_letters_num = len(letters)

    def __init__(self):
        super().__init__(self.lang, self.letters)

    def is_en_letter(self, text):
        text = text.replace(' ', '')
        return all(letter.upper() in self.letters for letter in text)

    def letters_num(self):
        return self.__en_letters_num

    @staticmethod
    def example():
        return 'English text example.'
