class House:
    def __init__(self, area=100, price=100000):
        self._area = area
        self._price = price

    def final_price(self, discount=0.1):
        return self._price * (1 - discount)


class SmallHouse(House):
    def __init__(self):
        super().__init__(area=40)


class Human:
    def_name = 'John'
    def_age = 24

    def __init__(self, name=def_name, age=def_age, money=0,
                 house: House = None):
        self.name = name
        self.age = age
        self._money = money
        self._house = house

    def info(self):
        print(f"Name: {self.name}")
        print(f"Age: {self.age}")
        if self._house:
            print(f"House area: {self._house._area} sq.m.")
            print(f"House price: {self._house.final_price()} USD")
        else:
            print("No house")
        print(f"Money: {self._money} USD")

    @staticmethod
    def default_info():
        print(f"Default name: {Human.def_name}")
        print(f"Default age: {Human.def_age}")

    def make_deal(self, house, price):
        self._money -= price
        self._house = house

    def earn_money(self, amount):
        self._money += amount

    def buy_house(self, house, discount=0.1):
        price = house.final_price(discount=discount)
        if self._money >= price:
            self.make_deal(house, price)
            print("Congratulations! You've just bought a house!")
        else:
            print("Sorry, you don't have enough money to buy this house.")
