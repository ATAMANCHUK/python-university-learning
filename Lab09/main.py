# -----------------------------------------------------------------
# Eleven labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

from first_task import Alphabet, EngAlphabet
from second_task import House, SmallHouse, Human
from third_task import Apple, AppleTree, Gardener
from fourth_task import *


# Intro

def print_intro(task_num):
    print('{:_^35}'.format(f' {task_num} Task '))


# First task

print_intro(1)
alphabet = EngAlphabet()
alphabet.print_alphabet()
print(alphabet.letters_num())
print(alphabet.is_en_letter('J'))
print(alphabet.is_ua_lang('Щ'))
print(EngAlphabet.example())

# Second task

print_intro(2)
Human.default_info()

human1 = Human(name='Bob', age=25)
human1.info()

small_house = SmallHouse()
human1.buy_house(small_house)
human1.earn_money(120000)
human1.buy_house(small_house)
human1.info()

# Third task

print_intro(3)
tree = AppleTree(3)

Gardener.apple_base(tree)

tree = AppleTree(10)
gardener = Gardener("Ivan", tree)

gardener.work()
gardener.work()
Gardener.apple_base(tree)
gardener.harvest()
gardener.work()
gardener.harvest()

Gardener.apple_base(tree)

# Fourth task

print_intro(4)
