import os
import csv
import time
import datetime
import matplotlib.pyplot as plt
import numpy as np


class KmrCsv:
    def __init__(self, ref='data.csv', num=1):
        self._file_ref = ref
        self._work_num = num

    def set_file_ref(self, ref):
        self._file_ref = ref

    def set_work_num(self, num):
        self._work_num = num

    def read_file(self):
        with open(self._file_ref, 'r', newline='') as file:
            reader = csv.reader(file)
            rows = list(reader)
            return rows

    def print_file_info(self):
        print(f'Control work number: {self._work_num}')
        print(f'Students count {len(self.read_file()) - 1}')


class Statistic:
    def __init__(self, work: KmrCsv):
        self._work = work

    def avg_stat(self):
        student_testing_list = self._work.read_file()

        quest_stat = []
        for question_num in range(5, 25):
            quest_grades = []
            for stud in student_testing_list:
                if '-' not in stud[question_num]:
                    quest_grades.append(
                        float(stud[question_num].replace(',', '.')))
            correct_len = len([i for i in quest_grades if i >= 0.5])
            correct_percent = round(correct_len / len(quest_grades) * 100, 3)
            incorrect_percent = round(100.0 - correct_percent, 3)
            quest_stat.append(
                [question_num - 4, correct_percent])

        return tuple(quest_stat)

    def marks_stat(self):
        student_testing_list = self._work.read_file()

        all_grades = []
        for stud in student_testing_list:
            all_grades.append(float(stud[4].replace(',', '.')))

        student_grades = dict()
        for i in range(11):
            student_grades[i] = all_grades.count(i)

        return student_grades

    def mark_per_time(self):
        student_testing_list = self._work.read_file()

        all_grades = []
        for stud in student_testing_list:
            all_grades.append(float(stud[4].replace(',', '.')))

        grades_per_time = dict()
        for minutes in range(21):
            grades = []
            grades_sum = 0
            student_id = 0
            i = 0
            for stud in student_testing_list:
                time_str = '%m/%d/%Y %I:%M'
                time_str_an = '%d %B %Y %H:%M %p'
                if stud[1].count('/') > 0:
                    start_test = datetime.datetime.strptime(stud[1], time_str)
                else:
                    start_test = datetime.datetime.strptime(stud[1],
                                                            time_str_an)
                if stud[2].count('/') > 0:
                    end_test = datetime.datetime.strptime(stud[2], time_str)
                else:
                    end_test = datetime.datetime.strptime(stud[2], time_str_an)
                test_time = end_test - start_test
                check_max_time = datetime.timedelta(minutes=float(25))
                check_min_time = datetime.timedelta(minutes=float(0))
                if test_time > check_max_time or test_time < check_min_time:
                    if str(test_time).find(','):
                        test_time = str(test_time)[
                                    str(test_time).find(',') + 5:]
                    print(i)
                    test_time = '00:' + test_time
                    time_str_an = '%H:%M:%S'
                    start_test = datetime.datetime.strptime('0:00:00',
                                                            time_str_an)
                    end_test = datetime.datetime.strptime(str(test_time),
                                                          time_str_an)
                    test_time = end_test - start_test
                comp_val = datetime.timedelta(minutes=float(f'{minutes}'))
                one_min = datetime.timedelta(minutes=float(1))
                zero_min = datetime.timedelta(minutes=float(0))
                time_diff = test_time - comp_val
                if test_time <= comp_val and zero_min <= time_diff <= comp_val:
                    grades.append(stud[4])
                    student_id = stud[0]

            for i in grades:
                grades_sum += float(i.replace(',', '.'))
            if len(grades) != 0:
                average_grade = grades_sum / len(grades)
            else:
                average_grade = 0

            grades_per_time[student_id] = average_grade

        return grades_per_time

    def best_marks_per_time(self, bottom_margin=0, top_margin=10):
        grader_per_time = self.mark_per_time()
        all_grades = dict()
        student_statistic = dict()
        for stud in self._work.read_file():
            grade = 0
            for question_num in range(5, 25):
                curr_grade = stud[question_num].replace(',', '.')
                if curr_grade != '-':
                    grade += float(curr_grade)
                else:
                    grade += 0
            all_grades[stud[0]] = grade

        for stud_id in all_grades:
            student_statistic[stud_id] = {
                'finish_grade': all_grades[stud_id],
                'avg_grade_per_minute': grader_per_time[stud_id]
            }

        filtered_students = []
        for stud_id in all_grades:
            if student_statistic[stud_id]['finish_grade'] <= top_margin:
                filtered_students.append([
                    stud_id,
                    student_statistic[stud_id]['finish_grade'],
                    student_statistic[stud_id]['avg_grade_per_minute']
                ])

        sorted_studs = filtered_students.sort(reverse=True, key=lambda x: x[2])
        top_five = sorted_studs[0:5]
        top_five_dict = dict()

        for stud in top_five:
            top_five_dict[stud[0]] = student_statistic[stud[0]]

        return top_five_dict


class Plots:
    def __init__(self, cat):
        self._catalog = cat

    def set_cat(self, cat):
        self._catalog = cat

    def avg_plot(self, y_correct_percent, x_question_num=None):
        y = np.array(y_correct_percent)
        if x_question_num is None:
            plt.plot(y, scalex=True, scaley=True)
        else:
            x = np.array(x_question_num)
            plt.plot(x, y, scalex=True, scaley=True)

        plt.savefig(f'./{self._catalog}/avg_plot.png')

    def marks_plot(self, x_grades, y_percent):
        x = np.array(x_grades)
        y = np.array(y_percent)
        plt.plot(x, y)
        plt.savefig(f'./{self._catalog}/marks_plot.png')

    def best_marks_plot(self, x_student_ids, y_avg_grade_per_minute):
        x = np.array(x_student_ids)
        y = np.array(y_avg_grade_per_minute)
        plt.plot(x, y)
        plt.savefig(f'./{self._catalog}/best_marks_plot.png')


class KmrWork(KmrCsv, Statistic, Plots):
    kmrs = dict()
    _cat = 'diagrams'

    def __init__(self, csv_ref, cmr_num):
        super().__init__(csv_ref, cmr_num)
        self.kmrs[cmr_num] = csv_ref

    @staticmethod
    def compare_csv(first_kmr, second_kmr):
        first_kmr: KmrWork
        second_kmr: KmrWork
        f_avg = [a for a in first_kmr.marks_stat()]
        print(f'Average grade for kmr 1: {first_kmr.marks_stat()')
        print(f'Average grade for kmr 2:')


