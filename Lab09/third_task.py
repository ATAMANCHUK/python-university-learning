class Apple:
    states = {0: 'Відсутнє',
              1: 'Цвітіння',
              2: 'Зелене',
              3: 'Червоне'}

    def __init__(self, apple_index):
        self._index = apple_index
        self._state = 0

    def grow(self):
        if self._state != 3:
            self._state += 1

    def is_ripe(self):
        if self._state == 3:
            return True
        else:
            return False


class AppleTree:
    def __init__(self, apples_count):
        self.apples = [Apple(index) for index in range(apples_count)]

    def grow_all(self):
        for apple in self.apples:
            apple.grow()

    def all_are_ripe(self):
        return all(apple.is_ripe() for apple in self.apples)

    def give_away_all(self):
        self.apples.clear()


class Gardener:
    def __init__(self, name, apple_tree: AppleTree):
        self.name = name
        self._tree = apple_tree

    def work(self):
        self._tree.grow_all()

    def harvest(self):
        if self._tree.all_are_ripe() and self._tree.apples:
            self._tree.give_away_all()
            print('Ur harvest have been picked')
        else:
            print('You do not have enough apples or your apples do not riped')

    @staticmethod
    def apple_base(tree: AppleTree):
        print('Info about ur apples')
        for state in Apple.states:
            count = sum(
                [1 for apple in tree.apples if apple._state == state])
            print(f"{Apple.states[state]}: {count}")
