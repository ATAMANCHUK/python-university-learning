# -----------------------------------------------------------------
# Seventh labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

from datetime import date
from datetime import datetime
from Person import Person

# Task 1 !!!

print('{:_^35}'.format(' Task 1 '))


def modify_user_data(filename: str) -> bool:
    """
    Modify UsersData.txt file

    Returns: <bool> object
    """
    file_data = dict()

    with open(filename, 'rt') as file:
        index = 0
        for line in file:
            clear_line = line.replace('\n', '')
            file_data[index] = clear_line
            index += 1

    data_titles = file_data.get(0).lower().split(' ')
    people = []

    for line in file_data:
        if line == 0:
            continue

        person_data = file_data.get(line).split(' ')
        person = dict(zip(data_titles, person_data))
        new_person = Person(
            person['surname'],
            person['first_name'],
            datetime.strptime(person['birth_date'], '%Y-%M-%d').date(),
            person['nickname']
        )
        people.append(new_person)

    with open(filename, 'wt') as file:
        text = list()
        text.append('Surname First_Name Full_Name Nickname Birth_Date Age \n')
        for p in people:
            text.append(f'{p.surname} {p.first_name} {p.get_fullname()} '
                        f'{p.nickname} {str(p.birth_date)} {p.get_age()} \n')
        file.writelines(text)

    return True


FILENAME = 'UsersData.txt'
print(f'Operation is {modify_user_data(FILENAME)}')
