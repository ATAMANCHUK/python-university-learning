# -----------------------------------------------------------------
# Person class
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# and by Python lover
# -----------------------------------------------------------------

# Imports

from datetime import date


class Person:
    """
    Class which realize person.

    Attributes:
        - surname: <str> object ( second name )
        - first_name: <str> object ( name )
        - birth_date: <datetime> object
        - nickname: <str> object

    Methods:
        - get_age() -> <str> object ( Returns person age )
        - get_fullname() -> <str> object ( Returns person fullname )
    """

    def __init__(self, surname: str, first_name: str, birth_date: date,
                 nickname: str = 'none'):
        # I created birth_date parameter with date type, because code
        # should be with SRP principe, this method haven't to initialize
        # variable and try additional recognize then transform to date type

        self.surname = surname
        self.first_name = first_name
        self.birth_date = birth_date
        self.nickname = nickname

    def get_age(self):
        """
        That method calculate person age in years.

        Return: <str> object
        """
        today_date = date.today()
        person_age = today_date.year - self.birth_date.year

        if today_date.month - self.birth_date.month < 0:
            person_age -= 1

        return str(person_age)

    def get_fullname(self):
        """
        That method return person fullname.

        Returns: <str> object
        """
        return self.first_name + ' ' + self.surname
