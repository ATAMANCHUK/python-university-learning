firstIntNumber = int(input("Enter first number: "))

secondIntNumber = int(input("Enter second number: "))

thirdFloatNumber = float(input("Enter third number: "))

fourthFloatNumber = float(input("Enter fourth number: "))

numberList = []

addVar = float(firstIntNumber + thirdFloatNumber)
subVar = firstIntNumber - secondIntNumber
multVar = firstIntNumber * secondIntNumber
divVar = thirdFloatNumber / fourthFloatNumber
degreeVar = firstIntNumber ** 2
wholeDegreeVar = secondIntNumber // 2
remainFromDivision = firstIntNumber % secondIntNumber

numberList.extend([addVar, subVar, multVar, divVar, degreeVar, wholeDegreeVar, remainFromDivision])

print(f"Кількість елементів списку - {len(numberList)}")
print(f"Парні елементи списку - {numberList[::2]}")

numberList[2], numberList[5] = numberList[5], numberList[2]
print(f"Список - {numberList}")

name = input('Ваше прізвище та ім`я: ')
print(f'Лабораторну роботу №1 виконав {name}.\n\nВисновок: на даній лабораторній роботі було проведене ознайомлення'
 '\nз базовими та початковими навичками роботи на мові програмування Python, а саме робота зі змінними різних типів,'
 '\nспособи введення та виведення інформації в консоль, можливості у використанні арифметичних операцій, як над'
 '\nдробовими так і над цілими числами, а також початкові кроки у використання та роботу зі списками. Виконано'
 '\nналаштування середовища розробки для подальшої роботи над лабораторними роботами. Роботу виконано в повному обсязі.')


