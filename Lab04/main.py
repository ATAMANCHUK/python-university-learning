# -----------------------------------------------------------------
# Fourth labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

import random
import math

# Task 1

print("_______________ Task 1 _______________")
numbers_list = [int(input("Number: ")) for i in range(int(input("Count of int elements: ")))]
numbers_list.reverse()
print(f"Max number in list is {max(numbers_list)}\nReversed list {numbers_list}")

# Task 2

print("_______________ Task 2 _______________")
numbers_list = [int(input("Number: ")) for i in range(int(input("Count of int elements: ")))]
additional_elements, negative_elements = [i for i in numbers_list if i > 0], [i for i in numbers_list if i < 0]
print(f"Additional elements: {additional_elements}\nNegative elements: {negative_elements}")

# Task 3

print("_______________ Task 3 _______________")
numbers_list = [i for i in range(1, 21)]
unpairs_indexes_sum = sum([i for i in numbers_list if not i.__index__() % 2 == 0])
print(f"Generated list {numbers_list}\nSum unpairs indexes {unpairs_indexes_sum}")

# Task 4

print("_______________ Task 4 _______________")
random_numbers = [random.randint(-100, 100) for i in range(1, 31)]
max_element, max_element_index = max(random_numbers), max(random_numbers).__index__()
unpairs_numbers = [i for i in random_numbers if not i % 2 == 0]
unpairs_numbers.sort(reverse=False)
print(f"Random numbers list {random_numbers}\nMax element is that list {max_element} and index {max_element_index}")
print(f"{unpairs_numbers if unpairs_numbers else 'We dont have unpair numbers'}")

# Task 5

print("_______________ Task 5 _______________")
random_numbers = [random.randint(-100, 100) for i in range(1, 31)]
print(f"Our list {random_numbers}")
for i in range(len(random_numbers) - 1):
    if random_numbers[i] < 0 and random_numbers[i + 1] < 0:
        print(f"{random_numbers[i]} and {random_numbers[i + 1]}")

# Task 6

print("_______________ Task 6 _______________")
random_numbers = [random.randint(-100, 100) for i in range(-5, 5)]
max_element = max(random_numbers)
quarters_lowers_numbers = [i ** 2 for i in random_numbers if i < max_element]
quarters_lowers_numbers.sort(reverse=True)
print(f"Starts list {random_numbers}\nMax element {max_element}\nQuarters lowers elements {quarters_lowers_numbers}")

# Task 7

print("_______________ Task 7 _______________")
random_numbers = [round(random.uniform(-100, 100), 2) for i in range(15)]
random_numbers += [random.randint(-100, 100) for b in range(15)]
print(f"Random numbers list {random_numbers}")
print(f"Minimal number module is {min([math.fabs(i) for i in random_numbers])}")

# Task 8

print("_______________ Task 8 _______________")
random_numbers = [round(random.uniform(-100, 100), 2) for i in range(15)]
random_numbers += [random.randint(-100, 100) for b in range(15)]
random.shuffle(random_numbers)
ten_numbers_lists = []

for i in range(0, 10):
    ten_numbers_lists.append(random_numbers[(3 * i):(3 * i + 3)])

print(f"Our random number {random_numbers}")
print(f"New ten lists {ten_numbers_lists}")
sorted_ten_lists_by_sum = sorted(ten_numbers_lists, key=lambda small_list: sum(small_list))
print(f"Sorted lists by absolute sum {sorted_ten_lists_by_sum}")
