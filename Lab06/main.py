# -----------------------------------------------------------------
# Sixth labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

import os
import csv
import pprint
import time
import datetime

# Task 1 !!!

print('{:_^35}'.format(' Task 1 '))
with open('numbers.txt', 'rt') as numbers_file:
    with open('sum_numbers.txt', 'w') as sum_numbers_file:
        nums_list = []
        nums_sum = 0
        for i in numbers_file:
            nums_list.append(int(i))
            nums_sum += int(i)
        sum_numbers_file.write(str(nums_sum))
        print(f'Nums {nums_list}\nNums sum {nums_sum}')

# Task 2

print('{:_^35}'.format(' Task 2 '))
nums_count = int(input('Nums count: '))
nums_list = [int(input('Number: ')) for i in range(nums_count)]
pair_nums = [i for i in nums_list if i % 2 == 0]
unpair_nums = [i for i in nums_list if i % 2 != 0]

with open('second_task_nums_info.txt', 'wt') as file:
    file.write(f'Pair nums: {pair_nums}\nUnpair nums: {unpair_nums}')

# Task 3

print('{:_^35}'.format(' Task 3 '))

sentences_list = []

with open('python_learning.txt', 'rt') as file:
    for line in file:
        sentences_list.append(line)
        print(line)

sentences_list.sort(reverse=True, key=lambda string: len(string))
print(sentences_list)

# Task 4

print('{:_^35}'.format(' Task 4 '))

sentences_list = []

with open('python_learning.txt', 'rt') as file:
    for line in file:
        print(line.replace('python', 'C language'))
        sentences_list.append(line.replace('python', 'C language'))

if not os.path.isdir('./C_language_dir'):
    os.mkdir('./C_language_dir')

with open('./C_language_dir/c_learning_true.txt', 'wt') as true_c_file:
    with open('./C_language_dir/c_learning_false.txt', 'wt') as false_c_file:
        true_lines = ''
        false_lines = ''
        print('\n{:+^35}'.format(' Questions '))
        for string in sentences_list:
            print(string)
            answer = input('Is that true? y or n: ')
            if 'y' in answer:
                true_lines += string
            elif 'n' in answer:
                false_lines += string.replace('can', 'cannot')
        true_c_file.write(true_lines)
        false_c_file.write(false_lines)

# Task 5

print('{:_^35}'.format(' Task 5 '))

FILENAME = 'guest_book.txt'


def log_new_guest(file, guest_name):
    now = datetime.datetime.today()
    logs = []
    if not os.path.isfile(file):
        with open(file, 'wt') as new_file:
            new_string = f'Date of starting logging: {now}'
            last_editing_str = f'Last edited: {now}'
            new_file.write(new_string + '\n' + last_editing_str)
    with open(file, 'rt') as guest_file:
        for line in guest_file:
            logs.append(line)
    with open(file, 'wt') as guest_file:
        last_editing_str = f'Last edited: {now}'
        new_guest_str = f'[{now}] New guest - {guest_name}'
        logs[1] = last_editing_str + '\n'
        logs.append(new_guest_str + '\n')
        guest_file.writelines(logs)


count_new_guests = int(input('How many new guests: '))
for i in range(count_new_guests):
    name = input('Hi! What is your name: ').capitalize()
    print(f'We glad to see you {name}')
    log_new_guest(FILENAME, name)

# Task 6

print('{:_^35}'.format(' Task 6 '))

PUBLIC_FILENAME = 'python_publication.txt'
LOGS_FILENAME = 'python_public_logs.txt'


def calc_exec_time_decorator(func):
    def wrapper(*args):
        start_time = time.time()
        result = func(*args)
        end_time = time.time()
        exec_time = end_time - start_time
        print(f'Execution program time {exec_time}')
        return result, exec_time

    return wrapper


@calc_exec_time_decorator
def analize_str_frequency(text: str, word: str):
    text, word = text.lower(), word.lower()
    return text.count(word)


@calc_exec_time_decorator
def analize_str_frequency(str_list: list, word: str):
    word = word.lower()
    count = 0
    for string in str_list:
        string = string.lower()
        count += string.count(word)
    return count


def log_new_analize(file, word, freq, exec_time):
    now = datetime.datetime.today()
    logs = []
    if not os.path.isfile(file):
        with open(file, 'wt') as new_file:
            new_string = f'Date of starting logging: {now}'
            instruction_str = 'first [] - time when was added this log\n'
            instruction_str += 'second [] - execution time'
            last_editing_str = f'Last edited: {now}'
            new_file.write(new_string + '\n' + instruction_str +
                           '\n' + last_editing_str)
    with open(file, 'rt') as log_file:
        for line in log_file:
            logs.append(line)
    with open(file, 'wt') as log_file:
        last_editing_str = f'Last edited: {now}'
        new_log_str = f'[{now}][{exec_time}] New log {word} - {freq} times'
        logs[3] = last_editing_str + '\n'
        logs.append(new_log_str + '\n')
        log_file.writelines(logs)


with open(PUBLIC_FILENAME, 'rt') as publication:
    word = 'a'
    text = []
    for string in publication:
        text.append(string)
    freq, exec_time = analize_str_frequency(text, word)
    print(f'Frequency in text {freq} times')
    log_new_analize(LOGS_FILENAME, word, freq, exec_time)

# Task 7

print('{:_^45}'.format(' Task 7 '))
FILENAME = 'marks.lab6.csv'

with open(FILENAME, 'rt', newline='') as csv_file:
    csvreader = csv.reader(csv_file)
    student_testing_list = [stud for stud in csvreader]

print(f'{len(student_testing_list)} students took test')
all_grades = []

for stud in student_testing_list:
    all_grades.append(float(stud[4].replace(',', '.')))
for i in range(11):
    print(f'{all_grades.count(i)} students has {i} grade')

print('{:+^45}'.format(' Average grades '))
for minutes in range(21):
    grades = []
    grades_sum = 0
    for stud in student_testing_list:
        time_str = '%m/%d/%Y %I:%M'
        time_str_an = '%d %B %Y %H:%M %p'
        if stud[1].count('/') > 0:
            start_test = datetime.datetime.strptime(stud[1], time_str)
        else:
            start_test = datetime.datetime.strptime(stud[1], time_str_an)
        if stud[2].count('/') > 0:
            end_test = datetime.datetime.strptime(stud[2], time_str)
        else:
            end_test = datetime.datetime.strptime(stud[2], time_str_an)
        test_time = end_test - start_test
        check_max_time = datetime.timedelta(minutes=float(25))
        check_min_time = datetime.timedelta(minutes=float(0))
        if test_time > check_max_time or test_time < check_min_time:
            if str(test_time).find(','):
                test_time = str(test_time)[str(test_time).find(',') + 5:]
            test_time = '00:' + test_time
            time_str_an = '%H:%M:%S'
            start_test = datetime.datetime.strptime('0:00:00', time_str_an)
            end_test = datetime.datetime.strptime(str(test_time), time_str_an)
            test_time = end_test - start_test
        comp_val = datetime.timedelta(minutes=float(f'{minutes}'))
        one_min = datetime.timedelta(minutes=float(1))
        zero_min = datetime.timedelta(minutes=float(0))
        time_diff = test_time - comp_val
        # print(test_time, comp_val)
        # print(time_diff)
        if test_time <= comp_val and zero_min <= time_diff <= comp_val:
            grades.append(stud[4])
    for i in grades:
        grades_sum += float(i.replace(',', '.'))
    if len(grades) != 0:
        average_grade = grades_sum / len(grades)
    else:
        average_grade = 0
    print(f'{minutes} minutes - average {round(average_grade, 3)} grade')

quest_stat = []
for question_num in range(5, 25):
    quest_grades = []
    for stud in student_testing_list:
        if '-' not in stud[question_num]:
            quest_grades.append(float(stud[question_num].replace(',', '.')))
    correct_len = len([i for i in quest_grades if i >= 0.5])
    correct_percent = round(correct_len / len(quest_grades) * 100, 3)
    incorrect_percent = round(100.0 - correct_percent, 3)
    print(rf'{correct_percent} %')
    quest_stat.append([question_num - 4, correct_percent, incorrect_percent])

with open('questions_statistic.txt', 'wt') as file:
    text_list = []
    for q in quest_stat:
        write_str = f'Question {q[0]} correct {q[1]} incorrect {q[2]}'
        text_list.append(write_str + '\n')
    file.writelines(text_list)

students_grades = []
for stud in student_testing_list:
    time_str = '%m/%d/%Y %I:%M'
    time_str_an = '%d %B %Y %H:%M %p'
    if stud[1].count('/') > 0:
        start_test = datetime.datetime.strptime(stud[1], time_str)
    else:
        start_test = datetime.datetime.strptime(stud[1], time_str_an)
    if stud[2].count('/') > 0:
        end_test = datetime.datetime.strptime(stud[2], time_str)
    else:
        end_test = datetime.datetime.strptime(stud[2], time_str_an)
    test_time = end_test - start_test
    check_max_time = datetime.timedelta(minutes=float(25))
    check_min_time = datetime.timedelta(minutes=float(0))
    if test_time > check_max_time or test_time < check_min_time:
        if str(test_time).find(','):
            test_time = str(test_time)[str(test_time).find(',') + 5:]
        test_time = '00:' + test_time
        time_str_an = '%H:%M:%S'
        start_test = datetime.datetime.strptime('0:00:00', time_str_an)
        end_test = datetime.datetime.strptime(str(test_time), time_str_an)
        test_time = end_test - start_test
    stud_grade = float(stud[4].replace(',', '.'))
    grade_time = stud_grade / float(test_time.total_seconds())
    stud_num = stud[0]
    students_grades.append([stud_num, stud_grade, str(test_time), grade_time])
    students_grades.sort(reverse=True, key=lambda stud: stud[3])

with open('questions_statistic.txt', 'at') as file:
    for i in range(5):
        stud_id = students_grades[i][0]
        time = students_grades[i][2]
        grade = students_grades[i][1]
        file.write(f'{i + 1} id {stud_id} time {time} grade {grade}' + '\n')
