# -----------------------------------------------------------------
# Third labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

ENGLISH_TEXT_FOR_TASKS = """But I must explain to you how all this: mistaken idea 
of denouncing pleasure: and praising pain was born and I will give you a complete 
account of the system, and expound Ford the actual: teachings of the great explorer 
of the truth, the master-builder of human happiness. No one: rejects, 
dislikes, or avoids pleasure itself. Because Alexander it is pleasure, but because those 
who do not know: how to pursue pleasure rationally encounter consequences that 
are extremely painful."""

print("\n_____________________ Our text _____________________")
print(ENGLISH_TEXT_FOR_TASKS)

# Task 1

print("_____________________ Task 1 _____________________")
lower_entered_letter = input("Enter your letter: ").lower()
upper_entered_letter = lower_entered_letter.upper()
print(ENGLISH_TEXT_FOR_TASKS.count(lower_entered_letter) +
      ENGLISH_TEXT_FOR_TASKS.count(upper_entered_letter))

# Task 2

print("_____________________ Task 2 _____________________")
print(f"Count of changes : to % is {ENGLISH_TEXT_FOR_TASKS.count(':')}")
print(f"Changed text: \n{ENGLISH_TEXT_FOR_TASKS.replace(':', '%')}")

# Task 3

print("_____________________ Task 3 _____________________")
print(f"Count of deleted dots is {ENGLISH_TEXT_FOR_TASKS.count('.')}")
print(f"Changed text: \n{ENGLISH_TEXT_FOR_TASKS.replace('.', '')}")

# Task 4

print("_____________________ Task 4 _____________________")
print(f"Count of changes a to o letter is {ENGLISH_TEXT_FOR_TASKS.count('a')}")
print(f"Changed text: \n{ENGLISH_TEXT_FOR_TASKS.replace('a', 'o')}")

# Task 5

print("_____________________ Task 5 _____________________")
print(f"Changed text: \n{ENGLISH_TEXT_FOR_TASKS.lower()}")

# Task 6

print("_____________________ Task 6 _____________________")
print(f"Count of o letters is {ENGLISH_TEXT_FOR_TASKS.count('o')}")
print(f"Changed text: \n{ENGLISH_TEXT_FOR_TASKS.replace('o', '')}")

# Task 7

print("_____________________ Task 7 _____________________")
OUR_SENTENCE = "пияка пішов пити пиво і після цього пішов спати"
print(OUR_SENTENCE[:int((len(OUR_SENTENCE) // 2))].replace('п', '*'))

# Task 8

print("_____________________ Task 8 _____________________")
entered_word = input("Please enter your word: ")
print(
    f"Word '{entered_word}' occurs in text {ENGLISH_TEXT_FOR_TASKS.count(entered_word)} times")

# Task 9

print("_____________________ Task 9 _____________________")
print(ENGLISH_TEXT_FOR_TASKS.title())

# Task 10

print("_____________________ Task 10 _____________________")
start_word_letter = input("Enter the start words letter: ")
end_word_letter = input("Enter the end words letter: ")
clear_text = ENGLISH_TEXT_FOR_TASKS.replace('\n', '').replace(':', '').replace(
    '.', '')
print(f"Words, start letter is {start_word_letter}: "
      f"{[i for i in clear_text.split(' ') if i[0] == start_word_letter]}")
print(f"Words, end letter is {end_word_letter}: "
      f"{[i for i in clear_text.split(' ') if i[-1] == end_word_letter]}")

# Task 11

print("_____________________ Task 11 _____________________")
vowel_letters = [i for i in ENGLISH_TEXT_FOR_TASKS if
                 i in ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']]
print(f"In text {len(vowel_letters)} vowel letters")

# Task 12

print("_____________________ Task 12 _____________________")
vowel_letters = [i for i in ENGLISH_TEXT_FOR_TASKS if
                 i not in ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']]
print(f"In text {len(vowel_letters)} consonant letters")

# Task 13

print("_____________________ Task 13 _____________________")
clear_text = ENGLISH_TEXT_FOR_TASKS.replace('\n', '').replace(':', '').replace(
    '.', '')
print(f"Words: {[i for i in clear_text.split(' ') if i[0].isupper() == True]}")
