from task4.Pets import Pet


class Dog(Pet):
    """
    This class realizes dog pet.

    Attributes:
        - name: <str> object
        - age: <str> object
        - breed: <str> object
        - nature: <str> object

    Methods:
        - voice(): <None>
        - wag_tail(): <None>

    Class Methods: none
    """
    _is_mammal = True
    nature = 'simple cute dog'
    breed = 'simple dog'

    def __init__(self, name: str, age: int):
        super(Dog, self).__init__(name, age, self._is_mammal,
                                  self.breed, self.nature)

    def voice(self):
        """
        That method describes voice action.

        Return: <None> ( print )
        """
        print(f'Dog {self.name} gav gav gav')

    def wag_tail(self):
        """
        That method describes wag tail action.

        Return: <None> ( print )
        """
        print(f'Dog {self.name} tail wagging')
