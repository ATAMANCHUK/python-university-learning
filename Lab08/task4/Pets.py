from task4.Pet import Pet


class Pets:
    """
    This class realizes pets array.

    Attributes: None

    Methods:
        - append_pet(): <None>
        - get_pets(): <list> ( all pets )
        - print_about_pets(): <None>

    Class Methods: none
    """
    def __init__(self, pets: list[Pet]):
        self.__my_pets = pets

    def append_pet(self, pet: Pet):
        """
        That method append new pet to your pets.

        Return: <None>
        """
        self.__my_pets.append(pet)

    def get_pets(self) -> list:
        """
        That method returns all your pets.

        Return: <list> ( pets )
        """
        return self.__my_pets

    def print_about_pets(self):
        """
        That method print info about all pets.

        Return: <None> ( print )
        """
        for pet in self.__my_pets:
            name, age, breed, nature, _is_mammal = pet.about_pet()
            mammal = 'mammal' if _is_mammal else 'not mammal'
            text = f'{nature} {breed} {name} {mammal}\n'.capitalize()
            print('[*] ' + text)
