from task4.Dog import Dog


class GermanShepherd(Dog):
    """
    This class GermanShepherd breed dog.

    Attributes:
        - name: <str> object
        - age: <str> object
        - breed: <str> object
        - nature: <str> object

    Methods:
        - voice(): <None>
        - look_for(): <int> object

    Class Methods: none
    """
    nature = 'confident, courageous, intelligent and gentle'
    breed = 'German Shepherd'

    def __init__(self, name, age):
        super(GermanShepherd, self).__init__(name, age)

    def voice(self) -> None:
        """
        That method describes voice action.

        Return: <None> ( print )
        """
        print(f'{self.breed} {self.name} RRR RRR RRR')

    def look_for(self, thing: object) -> int:
        """
        That method describes looking for action.

        Return: <int> ( thing id )
        """
        print(f'I looked for thing {id(thing)}')
        return id(thing)
