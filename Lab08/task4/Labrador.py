from task4.Dog import Dog


class Labrador(Dog):
    """
    This class Labrador breed dog.

    Attributes:
        - name: <str> object
        - age: <str> object
        - breed: <str> object
        - nature: <str> object

    Methods:
        - voice(): <None>
        - swim(): <None>

    Class Methods: none
    """
    nature = 'sweet-natured, intelligent, enthusiastic'
    breed = 'Labrador Retriever'

    def __init__(self, name, age):
        super(Labrador, self).__init__(name, age)

    def voice(self):
        """
        That method describes voice action.

        Return: <None> ( print )
        """
        print(f'{self.breed} {self.name} cute little gau')

    def swim(self):
        """
        That method describes swim action.

        Return: <None> ( print )
        """
        print(f'{self.breed} {self.name} swimming')
