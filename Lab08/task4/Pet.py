class Pet:
    """
    This class realizes pet.

    Attributes:
        - name: <str> object
        - age: <str> object
        - breed: <str> object
        - nature: <str> object

    Methods:
        - about_pet(): <tuple> object

    Class Methods: none
    """
    def __init__(self, name: str, age: int, is_mammal: bool,
                 breed: str, nature: str):
        self.name = name
        self.age = age
        self._is_mammal = is_mammal
        self.breed = breed
        self.nature = nature

    def about_pet(self) -> tuple:
        """
        That method describes pet.

        Return: <tuple> object ( all attributes )
        """
        return self.name, self.age, self.breed, self.nature, self._is_mammal
