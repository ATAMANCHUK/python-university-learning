class Car:
    """
    This class realizes car.

    Attributes:
        - brand: <str> object
        - model: <str> object
        - year: <int> object

    Methods:
        - accelerate() -> <int> object
        - brake() -> <bool> object
        - get_speed() -> <int> object

    Class Methods: none
    """

    def __init__(self, brand: str, model: str, year: int):
        self.brand = brand
        self.model = model
        self.year = year
        self.__speed = 0

    def accelerate(self) -> int:
        """
        That method accelerates your car up to 5 km/h.

        Return: <int> object ( car speed )
        """
        self.__speed += 5
        return self.__speed

    def brake(self) -> bool:
        """
        That method tries brakes your car down at 5 km/h.
        If the speed is more than 5, it will return True.

        Return: <bool> object
        """
        if self.__speed >= 5:
            self.__speed -= 5
            return True
        elif self.__speed <= 0:
            return False

    def get_speed(self) -> int:
        """
        That method returns car speed.

        Return: <int> object
        """
        return self.__speed
