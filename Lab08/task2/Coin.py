# Imports

import random as rand


# Class

class Coin:
    """
    This class realizes game coin.

    Attributes: none

    Methods:
        - toss() -> <str> object ( Tosses coin )
        - get_position() -> <str> object ( Returns coin position )

    Class Methods: none
    """

    def __init__(self):
        self.__sideup = 'edge'

    def toss(self) -> str:
        """
        That method tosses your coin.

        Return: <str> object ( tails or heads )
        """
        num = rand.randint(0, 1)
        if num == 0:
            self.__sideup = 'tails'
        elif num == 1:
            self.__sideup = 'heads'

        return self.__sideup

    def get_position(self):
        """
        That returns coin position.

        RReturn: <str> object ( tails or heads )
        """
        return self.__sideup
