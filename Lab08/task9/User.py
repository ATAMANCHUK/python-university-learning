class User:
    def __init__(self, first_name: str, last_name: str,
                 email: str, nickname: str, is_news_accepted: bool = False,
                 login_attempts: int = 0):

        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.nickname = nickname
        self.is_news_accepted = is_news_accepted
        self.__login_attempts = login_attempts

    def describe_user(self):
        print(f'User {self.first_name} {self.last_name}')

    def greeting_user(self):
        print(f'We are glad to see you {self.first_name}')

    def get_login_attempts(self):
        return self.__login_attempts

    def increment_login_attempts(self):
        self.__login_attempts += 1

    def reset_login_attempts(self):
        self.__login_attempts = 0
