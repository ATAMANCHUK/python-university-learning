from task9.User import User
from task9.Privileges import Privileges


class Admin(User):
    def __init__(self, first_name: str, last_name: str,
                 email: str, nickname: str, privileges: Privileges,
                 is_news_accepted: bool = False, login_attempts: int = 0):

        super().__init__(first_name, last_name, email, nickname,
                         is_news_accepted, login_attempts)

        self.privileges = privileges
