class Bank:
    """
    This class realizes bank.

    Attributes: none

    Methods:
        - deposit() -> None ( deposits some money )
        - withdraw() -> <int> object ( withdraws some money )
        - get_balance() -> <int> object ( returns your balance )

    Class Methods: none
    """

    def __init__(self):
        self.__balance = 0

    def deposit(self, count: int):
        """
        That method deposits some money to your bank bill.

        Return: None
        """
        self.__balance += count

    def withdraw(self, count: int) -> int:
        """
        That method withdraws some money from your bank bill.

        Return: <int> object
        """
        if self.__balance >= count:
            self.__balance -= count
            return count
        else:
            error_text = 'Your bill balance is lower then you wanna withdraw.'
            raise OverflowError(error_text)

    def get_balance(self) -> int:
        """
        That method gets your bank bill balance.

        Return: <int> object
        """
        return self.__balance
