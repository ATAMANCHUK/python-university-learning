# -----------------------------------------------------------------
# Eighth labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

from task2.Coin import Coin
from task3.Car import Car
from task4.Dog import Dog
from task4.GermanShepherd import GermanShepherd as Ger
from task4.Labrador import Labrador
from task4.Pets import Pets
from task5.NumbersBuffer import NumbersBuffer as Buffer
from task6.NameTooShortError import NameTooShortError
from task7.NumberConverter import NumberConverter
from task8.Shop import Shop
from task8.Discount import Discount
from task9.User import User
from task9.Admin import Admin
from task9.Privileges import Privileges


# Tasks

def main():
    # Task 2
    print('{:_^35}'.format(' Task 2 '))
    my_coin = Coin()
    print(f'[*] {my_coin.toss()}')

    # Task 3
    print('{:_^35}'.format(' Task 3 '))
    volvo = Car('volvo', 'XC60', 2015)
    for i in range(5):
        print(f'[+] Volvo speed: {volvo.accelerate()}')
    for i in range(5):
        if volvo.brake():
            print(f'[-] Volvo speed: {volvo.get_speed()}')

    # Task 4
    print('{:_^35}'.format(' Task 4 '))
    yasha, kiki, archi = Dog('Yasha', 11), Labrador('Kiki', 3), Ger('Busya', 5)
    Pets([yasha, kiki, archi]).print_about_pets()

    # Task 5
    print('{:_^35}'.format(' Task 5 '))
    nums_buf = Buffer(5)
    sums = nums_buf.add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    print(nums_buf.get_current_part())
    print(sums)

    # Task 6
    print('{:_^35}'.format(' Task 6 '))
    # name = 'Vyacheslav'
    # sec_name = 'Oleg'
    # NameTooShortError.check_name_length(name)
    # NameTooShortError.check_name_length(sec_name)

    # Task 7
    print('{:_^35}'.format(' Task 7 '))
    roman_number = NumberConverter.integer_to_roman(58)
    print(roman_number)
    print(NumberConverter.roman_to_integer(roman_number))

    # Task 8
    print('{:_^35}'.format(' Task 8 '))
    store = Shop('U Shepetrenka', 'Electronic')
    print(store.shop_name, store.store_type)
    store.describe_shop()
    store.open_shop()

    Shop('U Mene', 'Product Shop').describe_shop()
    Shop('ZDTU', 'Brain').describe_shop()
    Shop('KLAVA', 'Auto').describe_shop()

    store = Shop('ATB', 'Product')
    print(store.number_of_units)
    store.number_of_units = 10

    print(store.number_of_units)
    store.set_number_of_units(5)
    print(store.number_of_units)
    store.increment_number_of_units(3)
    print(store.number_of_units)

    store_discount = Discount(['Alcohol', 'Bread'], 'ATB', 'Products')
    print(store_discount.discount_products)

    all_store = Shop('Zolote Runo', 'Entertainment', 8)
    all_store.describe_shop()
    all_store.open_shop()

    # Task 9
    print('{:_^35}'.format(' Task 9 '))
    denys = User('Denys', 'Yakov', 'asdad123@gmail.com', 'DENCHIK', True)
    dmitro = User('Dmitro', 'Oleksa', 'dimka@gmail.com', 'DIMKA', True)
    denys.describe_user()
    denys.greeting_user()
    dmitro.describe_user()
    dmitro.greeting_user()

    denys.increment_login_attempts()
    denys.increment_login_attempts()
    print(denys.get_login_attempts())
    denys.reset_login_attempts()
    print(denys.get_login_attempts())

    # privileges = ['Allowed to add message', 'Allowed to delete users']
    # olya = Admin('Olya', 'Yava', 'olycka@gmail.com', 'OLYA', privileges, True)
    # olya.show_privileges()

    privileges = Privileges([
            "Allowed to add message",
            "Allowed to delete users",
            "Allowed to ban users"
        ])
    oleg = Admin('Oleg', 'Yava', 'olycka@gmail.com', 'Oleg', privileges, True)
    oleg.privileges.show_privileges()

# Main checker

if __name__ == '__main__':
    try:
        main()
    except Exception as Ex:
        print(f'We have Exception:\n[!] {Ex}')
