class NumbersBuffer:
    """
    This class realizes simple numbers buffer.
    You can save some numbers, the broder you can enter.
    If numbers is too much the method returns their sum.
    For example, if we have border of 5 elements, when we have 7
    elements then the method returns sum of 5 first elements and
    save 2 another elements.

    Attributes: None

    Methods:
        - add(): <list[int]> | <None> ( add numbers to the buffer )
        - get_current_part(): <list[int]> ( get all numbers in buffer )

    Class Methods: none
    """
    def __init__(self, max_elements: int):
        self.__max_elements = max_elements
        self.__numbers = []

    def add(self, *a: int) -> list[int] | None:
        """
        That method adds new numbers to the buffer.
        And this method check count of elements and if buffer need,
        the method sum elements to border and returns sum.

        Return: <list[int]> | <None>
        """
        self.__numbers.extend([*a])
        sums = []
        max_els = self.__max_elements
        while len(self.__numbers) >= max_els:
            sums.append(sum(self.__numbers[0:max_els]))
            self.__numbers = self.__numbers[max_els:]
        return sums if sums != [] else None

    def get_current_part(self) -> list[int]:
        """
        That method gets all numbers in buffer.

        Return: <list[int]>
        """
        return self.__numbers
