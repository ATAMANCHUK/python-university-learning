class Shop:
    """
    This class realizes shop.

    Attributes:
        - shop_name
        - store_type
        - number_of_units

    Methods:
        - describe_shop(): <None> ( describes shop and print 2 attributes )
        - open_shop(): <None> ( opens shop )
        - set_number_of_units(): <None>
        - increment_number_of_units(): <None>

    Class Methods: none
    """
    def __init__(self, shop_name: str, store_type: str,
                 number_of_units: int = 0):
        self.shop_name = shop_name
        self.store_type = store_type
        self.number_of_units = number_of_units

    def describe_shop(self):
        """
         That method describes shop.
         Prints all Shop attributes.

         Return: <None>
         """
        print(f'Shop {self.shop_name} has {self.store_type} store type')

    @staticmethod
    def open_shop():
        """
        That method open shop.
        Just prints a massage about open the Shop.

        Return: <None>
         """
        print('Shop has opened today')

    def set_number_of_units(self, count: int):
        """
         That method sets number of units.

         Return: <None>
         """
        self.number_of_units = count if count >= 0 else 0

    def increment_number_of_units(self, count: int):
        """
         That method increment number of units.

         Return: <None>
         """
        self.number_of_units += count if count > 0 else 0





