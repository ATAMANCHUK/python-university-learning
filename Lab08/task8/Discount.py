from task8.Shop import Shop


class Discount(Shop):
    """
    This class realizes Discount.

    Attributes:
        - shop_name
        - store_type
        - number_of_units
        - discount_products

    Methods:
        - describe_shop(): <None> ( describes shop and print 2 attributes )
        - open_shop(): <None> ( opens shop )
        - set_number_of_units(): <None>
        - increment_number_of_units(): <None>
        - get_discount_products(): <list>

    Class Methods: none
    """

    def __init__(self, discount_products: list, shop_name: str, store_type: str,
                 number_of_units: int = 0):
        super().__init__(shop_name, store_type, number_of_units)
        self.discount_products = discount_products

    def get_discount_products(self):
        """
        That method returns all products with discount.

        Return: <list>
        """
        return self.discount_products
