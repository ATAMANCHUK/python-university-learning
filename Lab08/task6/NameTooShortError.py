class NameTooShortError(ValueError):
    """
    This class realizes check-error class.
    This class can check the name length and if the name length
    is too short the method raises an NameTooShortError.

    Attributes: None

    Methods:
        - check_name_length(): <None> ( check str )

    Class Methods: none
    """

    def __init__(self, message='Name is too short'):
        super().__init__(message)

    @staticmethod
    def check_name_length(name) -> None:
        """
        That method checks the name length and if the length is
        too short the method raises an exception.

        Return: <None>
        """
        if len(name) < 10:
            raise NameTooShortError()
        else:
            print(f'Name {name} length is okay')
