class NumberConverter:
    """
    This class realizes numbers converter.

    Attributes: None

    Methods:
        - roman_to_integer(): <int>
        - integer_to_roman(): <str>

    Class Methods: none
    """

    @staticmethod
    def roman_to_integer(roman_num: str) -> int:
        """
         That method converts the roman number to integer.

         Return: <int>
         """
        nums_dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50,
                     'C': 100, 'D': 500, 'M': 1000}
        indx = 0
        nums_sum = 0
        for letter in roman_num:
            number = nums_dict.get(letter, False)
            if number:
                if indx > 0 and number > nums_dict.get(roman_num[indx - 1]):
                    nums_sum += (number - nums_dict.get(
                        roman_num[indx - 1]) * 2)
                else:
                    nums_sum += number
            indx += 1
        return nums_sum

    @staticmethod
    def integer_to_roman(dec_num: int) -> str:
        """
         That method converts the integer number to roman.

         Return: <str>
         """
        nums_dict = {
            1000: 'M',
            900: 'CM',
            500: 'D',
            400: 'CD',
            100: 'C',
            90: 'XC',
            50: 'L',
            40: 'XL',
            10: 'X',
            9: 'IX',
            5: 'V',
            4: 'IV',
            1: 'I'
        }

        roman_numeral = ''
        for value, numeral in nums_dict.items():
            while dec_num >= value:
                roman_numeral += numeral
                dec_num -= value
        return roman_numeral
