# -----------------------------------------------------------------
# Second labour work on the subject Python Programming Language
# Zhytomyr Polytechnic State University
# By Denys Atamanchyk, student of CyberSecurity group 21-2 ( 1 )
# -----------------------------------------------------------------

# Imports

from random import randint
import math
import calendar

# Task 1

print("_______________ Task 1 _______________")
random_numbers = [randint(1, 10) for i in range(3)]
print('List: ', random_numbers)
print(f"Integer nums in range [1,3]: {[x for x in random_numbers if 1 <= x <= 3]}")

# Task 2

print("_______________ Task 2 _______________")
year_number = int(input("Enter year: "))


def check_is_year_leap(year):
    if year % 4 == 0:
        if year % 100 == 0 and not year % 400 == 0:
            return False
        return True
    else:
        return False


print(f"Days in year {366 if check_is_year_leap(year_number) else 365}")

# Task 3

print("_______________ Task 3 _______________")
buying_sum = int(input("Enter sum: "))
DISCOUNT_SUM_3_PERCENT = 500
DISCOUNT_SUM_5_PERCENT = 1000
finally_sum = buying_sum * 0.95 if buying_sum > DISCOUNT_SUM_5_PERCENT else \
    buying_sum * 0.97 if buying_sum > DISCOUNT_SUM_3_PERCENT else buying_sum

print(f"Sum with discount: {finally_sum}")

# Task 4

print("_______________ Task 4 _______________")
numbers_list = [int(input("Enter a number: ")) for i in range(4)]
print(f"Nums: {numbers_list}\nCos number {min(numbers_list)}: {math.cos(min(numbers_list))}")

# Task 5

print("_______________ Task 5 _______________")
numbers_list = [int(input("Enter a number: ")) for i in range(3)]
print(f"Nums: {numbers_list}\nSin number {max(numbers_list)}: {math.sin(max(numbers_list))}")

# Task 6

print("_______________ Task 6 _______________")
triangle_side = int(input("Enter the isosceles triangle side: "))
triangle_base = int(input("Enter the isosceles triangle base: "))


def calculate_isosceles_triangle_area(base, side):
    triangle_h = math.sqrt(side ** 2 - (base / 2) ** 2)
    return triangle_base * 0.5 * triangle_h


triangle_area = calculate_isosceles_triangle_area(triangle_base, triangle_side)

print(f"{triangle_area / 2 if triangle_area % 2 == 0 else 'We cannot division 2'}")

# Task 7

print("_______________ Task 7 _______________")
month_number = int(input("Enter month number: "))
print(calendar.month_name[month_number])

# Task 8

print("_______________ Task 8 _______________")
numbers_list = [int(input("Enter number: ")) for i in range(3)]
positive_numbers_list = [i for i in numbers_list if i > 0]
print(f"Positive numbers: {positive_numbers_list}")

# Task 9

print("_______________ Task 9 _______________")
a_number = 1
b_number = 6
sum_numbers_from_a_to_b = sum([i for i in range(a_number, b_number + 1)])
print(sum_numbers_from_a_to_b)

# Task 10

print("_______________ Task 10 _______________")
a_number = 1
b_number = 6
sum_numbers_from_a_to_b = sum([i ** 2 for i in range(a_number, b_number + 1)])
print(sum_numbers_from_a_to_b)

# Task 11

print("_______________ Task 11 _______________")
a_number = int(input("Enter a A number ( A <= 200 ):"))
numbers_list = [i for i in range(a_number, 201)]
print(sum(numbers_list) / len(numbers_list))

# Task 12

print("_______________ Task 12 _______________")
a_number = int(input("Enter a A number ( A <= B ):"))
b_number = int(input("Enter a B number ( B >= A ):"))
i = a_number
numbers_sum = 0
while i < b_number:
    numbers_sum += i
    i += 1
print(numbers_sum)

# Task 13

print("_______________ Task 13 _______________")
a_number = int(input("Enter a number A ( 0 =< a =< 50 ): "))
print(sum([i ** 2 for i in range(a_number, 51)]))

# Task 14

print("_______________ Task 14 _______________")
n_number = int(input("Enter a number N ( N > 1 ): "))
i = 0
while i <= math.inf:
    formula_result = (5 ** i) > n_number
    if formula_result is True:
        print(f"K = {i} is the lowest number when condition 5 ^ K > N is True")
        break
    else:
        i += 1

# Task 15

print("_______________ Task 15 _______________")
n_number = int(input("Enter an N number: "))
first_number = 0

for i in range(n_number + 1):
    if i ** 2 > n_number:
        first_num = i
        break

print(f"First number = {first_number} ({first_number} ^ 2 = {first_number ** 2})")

# Task 16

print("_______________ Task 16 _______________")
n_number = int(input("Enter an N number: "))
first_number = 0
step_sum = 1
step = 1

while first_number < n_number:
    step_sum += step
    step += 2
    first_number = step_sum + step

print(f"First num = {first_number}")